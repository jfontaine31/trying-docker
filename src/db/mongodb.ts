import { MongoClient } from 'mongodb';

class MongoDbClientSingleton {
  private static instance: MongoClient;
  private constructor() {
    /** no-op **/
  }

  static async getInstance(): Promise<Readonly<MongoClient>> {
    if (this.instance === null || this.instance === undefined) {
      const client = new MongoClient(process.env?.MONGO_URL ?? '');
      console.log('Love');
      this.instance = await client.connect();
    }
    return this.instance;
  }
}

export default MongoDbClientSingleton;
