import { Router } from 'express';
import MongoDbClientSingleton from '../db/mongodb';

const peopleRouter = Router();

const COLLECTION_NAME: Readonly<string> = 'people';

peopleRouter.get('/', async (req, res) => {
  const mongoDbClient = await MongoDbClientSingleton.getInstance();
  const peopleCollection = mongoDbClient.db().collection(COLLECTION_NAME);
  res.json(peopleCollection.find({}));
});

// peopleRouter.put('/', (req, res) => {});

export default peopleRouter;
