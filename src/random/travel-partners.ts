const amex = [
  'AeroMexico, 1:1.6',
  'Air Canada',
  'Alitalia',
  'Aer Lingus, 1:1',
  'Air France',
  'All Nippon Airways',
  'Avianca LifeMiles',
  'British Airways',
  'Cathay Pacific Asia Miles',
  'Delta Airlines',
  'El Al Israel Airlines',
  'Emirates',
  'Etihad Airways',
  'Hawaiian Airlines',
  'Iberia Plus',
  'JetBlue, 1:0.8',
  'Singapore Airlines',
  'Qantas',
  'Virgin Atlantic',
  'Hilton, 1:2',
];
const capitalOne = [
  'Aeromexico',
  'Aeroplan',
  'Asia Miles',
  'Avianca LifeMiles',
  'British Airways',
  'Choice Privileges®2',
  'Emirates',
  'Etihad Airways',
  'Finnair Plus',
  'Flying Blue',
  'Qantas',
  'Singapore Airlines',
  'TAP Miles&Go',
  'Turkish Airlines Miles&Smiles',
  'Virgin Atlantic',
];

enum BankName {
  CHASE = 'chase',
  AMEX = 'amex',
  CAPITAL_ONE = 'capitalone',
  CITI = 'citi',
}

enum TransferTime {
  INSTANT,
  ONE_DAY,
  TWO_DAY,
}
class TransferRatio {
  readonly fromRatio: number;
  readonly toRatio: number;
  constructor(ratio: string) {
    [this.fromRatio, this.toRatio] = this.parseRatio(ratio);
  }

  private parseRatio(ratio: string): [number, number] {
    const [fromRatioStr, toRatioStr] = ratio.split(':');
    let [fromRatio, toRatio] = [Number(fromRatioStr), Number(toRatioStr)];
    toRatio = toRatio / fromRatio;
    fromRatio = fromRatio / fromRatio;

    return [fromRatio, toRatio];
  }
}

class TransferPartner {
  constructor(
    public name: string,
    public transferRatio: TransferRatio,
    public transferTime: TransferTime,
  ) {}
}
interface TravelPartnersMap {
  [partner: string]: BankName[];
}

// function buildPartners(
//   travelProvider: TravelProvider,
//   partners: TravelPartnersMap,
// ): TravelPartnersMap {
//   return travelProvider.providers.reduce((retPartners, partner) => {
//     if (!retPartners.hasOwnProperty(partner)) {
//       retPartners[partner] = [];
//     }
//     retPartners[partner].push(travelProvider.name);
//     return retPartners;
//   }, partners);
// }

// console.log(overlapKeys, partners);
