import express from 'express';
import peopleRouter from './routes/people';

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.use(express.json());

app.use('/people', peopleRouter);

app.get('/', (req, res) => {
  res.send('Hello World ive been updated yoooo');
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});
