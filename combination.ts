/**
 *
 * */

function comb(
  str: string,
  result = '',
  arr: string[] = [],
  map: { [key: string]: string[] } = {},
): string[] {
  if (str in map) {
    return map[str];
  }
  if (str === '') {
    if (!(str in map)) map[str] = [];

    map[str].push(result);
    return arr;
  }
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    const left = str.slice(0, i);
    const right = str.slice(i + 1);
    comb(left + right, result + char, arr, map);
  }
  return map[str];
}

console.log(comb('abcd'));
